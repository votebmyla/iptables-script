#!/bin/bash

FILE="./tasix.full.txt"

# Блокировать все новые входящие запросы
iptables -P INPUT DROP

# Разрешить все исходящие запросы
iptables -P OUTPUT ACCEPT

# Разрешить уже установленные входящие запросы
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

# Разрешить все входящие запросы на порты 22, 80 и 443 из подсетей указанных в файле
while IFS= read -r subnet
do
  iptables -A INPUT -p tcp --dport 22 -s "$subnet" -j ACCEPT
  iptables -A INPUT -p tcp --dport 80 -s "$subnet" -j ACCEPT
  iptables -A INPUT -p tcp --dport 443 -s "$subnet" -j ACCEPT
done < "$FILE"

